geru_challenge README
==================

Getting Started
---------------

- cd <directory containing this file>

- $VENV/bin/pip install -e .

- $VENV/bin/initialize_geru_challenge_db development.ini

- $VENV/bin/pserve development.ini

