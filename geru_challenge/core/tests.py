from geru_challenge.quotes.tests import (
    SAMPLE_QUOTES_RESPONSE, get_quotes_list_from_sample_response
)
import html
from pyramid import testing
import transaction
import unittest


def dummy_request(dbsession, path='/'):
    return testing.DummyRequest(dbsession=dbsession, path=path)


class BaseTest(unittest.TestCase):
    def setUp(self):
        from geru_challenge import main

        self.config = testing.setUp(settings={
            'sqlalchemy.url': 'sqlite:///:memory:'
        })
        self.config.include('.models')
        settings = self.config.get_settings()
        app = main({}, **settings)
        from webtest import TestApp
        self.testapp = TestApp(app)
        from .models import (
            get_engine,
            get_session_factory,
            get_tm_session,
            )

        self.engine = get_engine(settings)
        session_factory = get_session_factory(self.engine)

        self.session = get_tm_session(session_factory, transaction.manager)

    def init_database(self):
        from .models.meta import Base
        Base.metadata.create_all(self.engine)

    def tearDown(self):
        from .models.meta import Base

        testing.tearDown()
        transaction.abort()
        Base.metadata.drop_all(self.engine)


class CoreViewsTests(BaseTest):
    def test_root(self):
        res = self.testapp.get('/', status=200)
        self.assertTrue(b'Desafio Web 1.0' in res.body)

    def test_quotes(self):
        res = self.testapp.get('/quotes/', status=200)
        quotes = get_quotes_list_from_sample_response(SAMPLE_QUOTES_RESPONSE)
        body = html.unescape(res.body.decode('utf-8'))
        valid_quote = True
        for quote in quotes:
            valid_quote = valid_quote and quote in body
            if not valid_quote:
                break
        self.assertTrue(valid_quote)

    def test_quote(self):
        res = self.testapp.get('/quotes/0/', status=200)
        quotes = get_quotes_list_from_sample_response(SAMPLE_QUOTES_RESPONSE)
        expected_quote = quotes[0]
        self.assertIn(expected_quote, res.body.decode('utf-8'))

    def test_random_quote(self):
        res = self.testapp.get('/quotes/random/', status=200)
        quotes = get_quotes_list_from_sample_response(SAMPLE_QUOTES_RESPONSE)
        body = html.unescape(res.body.decode('utf-8'))
        valid_quote = False
        for quote in quotes:
            valid_quote = valid_quote or (quote in body)
            if valid_quote:
                break
        self.assertTrue(valid_quote)
