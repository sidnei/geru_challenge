from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.schema import MetaData
from collections import OrderedDict
from sqlalchemy.types import DateTime, Date

# Recommended naming convention used by Alembic, as various different database
# providers will autogenerate vastly different names making migrations more
# difficult. See: http://alembic.zzzcomputing.com/en/latest/naming.html
NAMING_CONVENTION = {
    "ix": 'ix_%(column_0_label)s',
    "uq": "uq_%(table_name)s_%(column_0_name)s",
    "ck": "ck_%(table_name)s_%(constraint_name)s",
    "fk": "fk_%(table_name)s_%(column_0_name)s_%(referred_table_name)s",
    "pk": "pk_%(table_name)s"
}

metadata = MetaData(naming_convention=NAMING_CONVENTION)

class Base(object):
    def __json__(self, request):
        result = OrderedDict()
        for c in self.__table__.columns:
            if getattr(self, c.name):
                if isinstance(c.type, DateTime) or isinstance(c.type, Date):
                    result[c.name] = getattr(self, c.name).isoformat()
                elif c.name.endswith('_id'):
                    result[c.name] = getattr(self, c.name).replace('_id', '')
                else:
                    result[c.name] = getattr(self, c.name)
        return result

Base = declarative_base(metadata=metadata, cls=Base)
