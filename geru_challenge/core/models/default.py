import json
from sqlalchemy import (
    Column,
    DateTime,
    ForeignKey,
    Index,
    Integer,
    String,
    Text,
)

from .meta import Base


class Session(Base):
    __tablename__ = 'session'
    id = Column(Integer, primary_key=True)
    key = Column(String(length=128), unique=True)
    _data = Column(Text)
    created_at = Column(DateTime)

    def set_data(self, data):
        self._data = json.dumps(data)

    def get_data(self):
        return json.loads(self._data)


class PageHit(Base):
    __tablename__ = 'pagehit'
    id = Column(Integer, primary_key=True)
    session = Column(Integer, ForeignKey('session.id'))
    timestamp = Column(DateTime)
    path = Column(String(length=2000))


Index('session_key_index', Session.key, unique=True)
Index('pagehit_path_index', PageHit.path)
