from datetime import datetime

from .models import PageHit, Session


def simple_tween_factory(handler, registry):
    def simple_tween(request):
        response = handler(request)

        session_key = request.cookies.get('session')
        if not session_key:
            request.session.invalidate()
        dbsession = request.dbsession
        now = datetime.now()

        session = dbsession.query(Session).filter(Session.key == session_key).first()
        if not session:
            session = Session(key=session_key, created_at=now)
            session.set_data(request.session)
            dbsession.add(session)
            dbsession.flush()
        page_hit = PageHit(session=session.id, timestamp=now, path=request.path)
        dbsession.add(page_hit)

        return response

    return simple_tween
