from geru_challenge.quotes.client import QuotesClient

api = None


def get_api_client():
    global api
    api = api or QuotesClient(endpoint='1c22eh3aj8.execute-api.us-east-1.amazonaws.com/challenge/')
    return api


def get_quotes():
    api_client = get_api_client()
    response = api_client.get_quotes()
    return response.get('quotes')


def get_quote(quote_number):
    api_client = get_api_client()
    response = api_client.get_quote(quote_number)
    return response.get('quote')
