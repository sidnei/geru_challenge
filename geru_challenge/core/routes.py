def includeme(config):
    config.add_static_view('static', 'static', cache_max_age=3600)
    config.add_route('home', '/')
    config.add_route('quotes', '/quotes/')
    config.add_route('quote', '/quotes/{id:\d+}/')
    config.add_route('random_quote', '/quotes/random/')

    # API's routes
    config.add_route('sessions', '/sessions/')
    config.add_route('session', '/sessions/{id:\d+}/')
    config.add_route('impressions', '/impressions/')
    config.add_route('impression', '/impressions/{id:\d+}/')
    config.add_route('session_impressions', '/sessions/{id:\d+}/impressions/')
