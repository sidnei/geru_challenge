from pyramid.view import view_config, view_defaults
import dateutil.parser

from ..models import Session, PageHit


@view_defaults(route_name='session', renderer='json')
class SessionViews(object):
    notfound_response = {
        'status': '404',
        'title': 'Not found.',
        'details': 'Requested resource does not exist.',
    }
    error_response = {
        'status': '500',
        'title': ' .',
        'details': 'Requested resource does not exist.',
    }
    _success_response = {
        'id': '{}',
        'status': '200',
    }
    success_create_response = _success_response.copy()
    success_create_response['title'] = 'Successfully created.'
    success_create_response['details'] = 'Session "{}" was created.'
    success_update_response = _success_response.copy()
    success_update_response['title'] = 'Successfully updated.'
    success_update_response['details'] = 'Session "{}" was updated.'
    success_delete_response = _success_response.copy()
    success_delete_response['title'] = 'Successfully deleted.'
    success_delete_response['details'] = 'Session "{}" was deleted '

    def __init__(self, request):
        self.request = request
        self.dbsession = request.dbsession

    @view_config(request_method='GET', route_name='sessions')
    def list(self):
        return self.dbsession.query(Session).all()

    @view_config(request_method='GET')
    def retrieve(self):
        session_id = self.request.matchdict['id']
        try:
            one = self.dbsession.query(Session).filter(Session.id == session_id).one()
        except:
            return self.notfound_response
        return one

    @view_config(request_method='PUT', route_name='sessions')
    def create(self):
        try:
            one = Session()
            one.key = self.request.POST['key']
            one.set_data(self.request.POST['data'])
            one.created_at = dateutil.parser.parse(self.request.POST['created_at'])
            self.dbsession.add(one)
            self.dbsession.flush()
        except self.dbsession.IntegrityError as e:
            return self.notfound_response
        response = self.success_create_response
        response['id'] = response['id'].format(one.id)
        response['details'] = response['details'].format(one.id)
        return self.success_create_response

    @view_config(request_method='POST')
    def update(self):
        session_id = self.request.matchdict['id']
        try:
            one = self.dbsession.query(Session).filter(Session.id == session_id).one()
            for c, v in self.request.POST.items():
                if c in Session.__table__.columns:
                    setattr(one, c, v)
            self.dbsession.update(one)
        except:
            return self.notfound_response
        return self.success_update_response

    @view_config(request_method='DELETE')
    def delete(self):
        session_id = self.request.matchdict['id']
        try:
            one = self.dbsession.query(Session).filter(Session.id == session_id).one()
            self.dbsession.delete(one)
        except:
            return self.notfound_response
        return self.success_delete_response


@view_config(route_name='impressions', renderer='json')
def impressions(request):
    return request.dbsession.query(PageHit).all()


@view_config(route_name='impression', renderer='json')
def impression(request):
    pagehit_id = request.matchdict['id']
    try:
        one = request.dbsession.query(PageHit).filter(PageHit.id == pagehit_id).one()
    except:
        return notfound_api_response
    return one


@view_config(route_name='session_impressions', renderer='json')
def session_impressions(request):
    session_id = request.matchdict['id']
    try:
        one = request.dbsession.query(Session).filter(Session.id == session_id).one()
    except:
        return notfound_api_response
    return request.dbsession.query(PageHit).filter(PageHit.session == one.id).all()
