from pyramid.view import notfound_view_config, view_config
from random import randint
from .. import services


@notfound_view_config(renderer='../templates/404.jinja2', append_slash=True)
def notfound_view(request):
    request.response.status = 404
    return {}


@view_config(route_name='home', renderer='../templates/home.jinja2')
def home(request):
    return {}


@view_config(route_name='quotes', renderer='../templates/quotes_list.jinja2')
def list_quotes(request):
    quotes = services.get_quotes()
    return {'quotes': quotes}


@view_config(route_name='quote', renderer='../templates/quote_detail.jinja2')
def show_quote(request):
    quote_number = request.matchdict['id']
    quote = services.get_quote(quote_number)
    return {'quote': quote}


@view_config(route_name='random_quote', renderer='../templates/quote_detail.jinja2')
def show_random_quote(request):
    quotes = services.get_quotes()
    quote_number = randint(0, len(quotes) - 1)
    quote = quotes[quote_number]
    return {'quote': quote}
