from pyramid.config import Configurator
from pyramid.session import SignedCookieSessionFactory


def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    config = Configurator(settings=settings)
    config.include('pyramid_jinja2')
    config.include('geru_challenge.core.models')
    config.include('geru_challenge.core.routes')
    session_factory = SignedCookieSessionFactory('itsaseekreet', reissue_time=None)
    config.set_session_factory(session_factory)
    config.scan()
    return config.make_wsgi_app()
