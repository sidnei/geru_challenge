import json
import requests
from urllib import parse


class ApiClient(object):
    endpoint = u''
    protocol = u'https'
    base_url = u'{protocol}://{endpoint}/'

    def __init__(self, *args, **kwargs):
        self.protocol = kwargs.get(u'protocol', self.protocol)
        self.endpoint = kwargs.get(u'endpoint', self.endpoint)
        self._headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json; charset=utf-8',
        }
        self._url = self.base_url.format(
            protocol=self.protocol,
            endpoint=self.endpoint,
        )

    def _to_python(self, response):
        return json.loads(response, encoding='utf-8')

    def _make_options(self, options):
        return u'?' + parse.urlencode(options)

    def raw(self, method, url, **params):
        response = requests.request(
            method, url, headers=self._headers, **params
        )
        return response.content.decode('utf-8')

    def call(self, method, url, **params):
        content = self.raw(method, url, **params)
        return self._to_python(content)

    def get(self, api_object, **options):
        url = self._url + api_object + self._make_options(options)
        return self.call('GET', url)

    def post(self, api_object, **data):
        url = self._url + api_object
        return self.call('POST', url, data=data)

    def put(self, api_object, object_id, **data):
        url = self._url + api_object + '/%s' % object_id
        return self.call('PUT', url, data=data)

    def delete(self, api_object, object_id):
        url = self._url + api_object + '/%s' % object_id
        return self.call('DELETE', url)


class QuotesClient(ApiClient):
    api_object = 'quotes'

    def get_quotes(self):
        return self.get(self.api_object)

    def get_quote(self, quote_number):
        api_object = '{resource}/{id}'.format(
            resource=self.api_object,
            id=quote_number
        )
        return self.get(api_object)
