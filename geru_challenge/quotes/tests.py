import json
from pyramid import testing
import transaction
import unittest
from unittest.mock import patch

from geru_challenge.quotes.client import QuotesClient


SAMPLE_QUOTES_RESPONSE = bytes('''{
    "quotes": [
        "Beautiful is better than ugly.", 
        "Explicit is better than implicit.", 
        "Simple is better than complex.", 
        "Complex is better than complicated.", 
        "Flat is better than nested.", 
        "Sparse is better than dense.", 
        "Readability counts.", 
        "Special cases aren't special enough to break the rules.", 
        "Although practicality beats purity.", 
        "Errors should never pass silently.", 
        "Unless explicitly silenced.", 
        "In the face of ambiguity, refuse the temptation to guess.", 
        "There should be one-- and preferably only one --obvious way to do it.", 
        "Although that way may not be obvious at first unless you're Dutch.", 
        "Now is better than never.", 
        "Although never is often better than *right* now.", 
        "If the implementation is hard to explain, it's a bad idea.", 
        "If the implementation is easy to explain, it may be a good idea.", 
        "Namespaces are one honking great idea -- let's do more of those!"
    ]
}''', encoding='utf-8')


def get_quote_response_from_sample_response(sample, quote_number):
    parsed_response = json.loads(sample.decode('utf-8'))
    quotes = parsed_response.get('quotes')
    quote_response_string = json.dumps({'quote': quotes[quote_number]})
    return bytes(quote_response_string, encoding='utf-8')


def get_quotes_list_from_sample_response(sample):
    parsed_response = json.loads(sample.decode('utf-8'), encoding='utf-8')
    return parsed_response.get('quotes')


def dummy_request(dbsession):
    return testing.DummyRequest(dbsession=dbsession)


class BaseTest(unittest.TestCase):
    def setUp(self):
        from geru_challenge import main

        self.config = testing.setUp(settings={
            'sqlalchemy.url': 'sqlite:///:memory:'
        })
        self.config.include('geru_challenge.core.models')
        settings = self.config.get_settings()
        app = main({}, **settings)
        from webtest import TestApp
        self.testapp = TestApp(app)

        from geru_challenge.core.models import (
            get_engine,
            get_session_factory,
            get_tm_session,
        )

        self.engine = get_engine(settings)
        session_factory = get_session_factory(self.engine)

        self.session = get_tm_session(session_factory, transaction.manager)

    def init_database(self):
        from geru_challenge.core.models.meta import Base
        Base.metadata.create_all(self.engine)

    def tearDown(self):
        from geru_challenge.core.models.meta import Base

        testing.tearDown()
        transaction.abort()
        Base.metadata.drop_all(self.engine)


class TestApiClient(unittest.TestCase):

    def setUp(self):
        endpoint = '1c22eh3aj8.execute-api.us-east-1.amazonaws.com/challenge/'
        self.client = QuotesClient(endpoint=endpoint)

    @patch('geru_challenge.quotes.client.requests.request')
    def test_get_quotes(self, mock_request):
        mock_request.return_value.ok = True
        mock_request.return_value.content = SAMPLE_QUOTES_RESPONSE
        data = self.client.get_quotes()
        test_case = 'Beautiful is better than ugly.'
        self.assertIn(test_case, data.get('quotes'))

    @patch('geru_challenge.quotes.client.requests.request')
    def test_get_quote(self, mock_request):
        mock_request.return_value.ok = True
        quote_number = 1
        mock_request.return_value.content = get_quote_response_from_sample_response(
            SAMPLE_QUOTES_RESPONSE, quote_number
        )
        data = self.client.get_quote(quote_number)
        test_case = 'Explicit is better than implicit.'
        self.assertEqual(test_case, data.get('quote'))
